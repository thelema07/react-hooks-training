import React from "react";
import "./App.css";
import { ScrollCheck } from "./components/ScrollCheck/ScrollCheck";

function App() {
  return (
    <div className="App">
      <ScrollCheck />
    </div>
  );
}

export default App;
