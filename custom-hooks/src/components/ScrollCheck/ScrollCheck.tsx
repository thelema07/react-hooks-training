import React from "react";
import useScrollDetection from "../../hooks/scroll-detection";

export const ScrollCheck = () => {
  const scrollPosition = useScrollDetection();
  const wrapperStyles = {
    height: "5000px",
  };
  const displayStyles = {
    position: "fixed",
    width: "100%",
    top: "50%",
    transform: "translateY(-50%)",
    fontSize: "20px",
    textAlign: "center",
  };
  return (
    <>
      <div style={wrapperStyles}>
        <div
          style={{
            position: "fixed",
            width: "100%",
            top: "50%",
            transform: "translateY(-50%)",
            fontSize: "20px",
            textAlign: "center",
          }}
        >
          <h1>This is my custom hook for Scroll!!</h1>
          <p>{scrollPosition !== null ? scrollPosition : 0}</p>
        </div>
      </div>
    </>
  );
};
