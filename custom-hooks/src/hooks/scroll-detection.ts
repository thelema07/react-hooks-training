import { useState, useEffect } from "react";

type Option = Number | null;

export default () => {
  const [scrollPosition, setScrollPosition] = useState<Option>(null);
  useEffect(() => {
    const handleScroll = () => setScrollPosition(window.scrollY);
    document.addEventListener("scroll", handleScroll);
    return () => {
      document.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return scrollPosition;
};
